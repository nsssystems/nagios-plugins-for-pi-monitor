# README #

This is a nagios plugin designed to integrate output from the [sampleSensors.py](https://bitbucket.org/nsssystems/grove-integration-for-pi-monitor) program with [nagios](https://www.nagios.org/).

### Set Up ###

Follow the directions to install the [sampleSensors.py](https://bitbucket.org/nsssystems/grove-integration-for-pi-monitor) program. 

Install nginx packages:

    sudo apt-get install nginx ntp

Customize nginx so that it serves files from /var/sensors instead of /var/www/html. A remote nagios server can now read these values using the nagios plugin from this repository.

Further nagios integration is left as an exercise for the reader.

### Contact Information ###

* This is a sample program developed by the UMass Enterprise Applications team.
* for more information, contact [Mark Scarbrough](marks@umass.edu)